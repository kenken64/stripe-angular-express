import { Component, OnInit} from '@angular/core';
//import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { StripeService, Element as StripeElement,ElementOptions, ElementsOptions } from "@nomadreservations/ngx-stripe";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  stripeKey = '';
  error: any;
  complete = false;
  element: StripeElement;
  cardOptions: ElementOptions = {
    style: {
      base: {
        iconColor: '#276fd3',
        color: '#31325F',
        lineHeight: '40px',
        fontWeight: 300,
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '18px',
        '::placeholder': {
          color: '#CFD7E0'
        }
      }
    }
  };

  elementsOptions: ElementsOptions = {
    locale: 'en'
  };

  constructor(
    private _stripe: StripeService
  ) {}

  ngOnInit(){

  }

  cardUpdated(result) {
    this.element = result.element;
    this.complete = result.card.complete;
    this.error = undefined;
  }

  keyUpdated() {
    this._stripe.changeKey(this.stripeKey);
  }

  async payNow() {
    const response = await fetch('http://localhost:3000/payment-intents', {
      method: 'POST',
      body: JSON.stringify({amount: 10000, currency: 'sgd'}), // 100 sgd
      headers: {'content-type': 'application/json'},
    });

    const body = await response.json();
    console.log(body);
    this._stripe.handleCardPayment(body.paymentIntent.client_secret, this.element, { payment_method_data: {
      billing_details: {name: 'Kenneth Phang'}
    }}).subscribe(result => {
      if(result.error) {
        console.error('got stripe error', result.error);
      } else {
        console.log('payment succeeded');
      }
    });
  }
}
