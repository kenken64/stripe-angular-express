require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
stripe.customers.list({apiVersion: process.env.STRIPE_API_VERSION});
const ngrok = process.env.NODE_ENV ? require('ngrok') : null;
const app = express();

const paymentMethods = [
    'card', // many (https://stripe.com/docs/currencies#presentment-currencies)
];

const shippingOptions= [
    {
      id: 'free',
      label: 'Free Shipping',
      detail: 'Delivery within 5 days',
      amount: 0,
    },
    {
      id: 'express',
      label: 'Express Shipping',
      detail: 'Next day delivery',
      amount: 500,
    },
  ]

// Setup useful middleware.
app.use(
  bodyParser.json({
    // We need the raw body to verify webhook signatures.
    // Let's compute it only when hitting the Stripe webhook endpoint.
    verify: function(req, res, buf) {
      if (req.originalUrl.startsWith('/webhook')) {
        req.rawBody = buf.toString();
      }
    },
  })
);
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, '../../public')));
app.use(cors());

// Create the PaymentIntent on the backend.
app.post('/payment-intents', async (req, res, next) => {
    let {currency, amount} = req.body;
    console.log(currency);
    console.log(amount);
    try {
      const paymentIntent = await stripe.paymentIntents.create({
        amount,
        currency,
        payment_method_types: paymentMethods,
      });
      console.log(paymentIntent);
      return res.status(200).json({paymentIntent});
    } catch (err) {
      return res.status(500).json({error: err.message});
    }
});


// Expose the Stripe publishable key and other pieces of config via an endpoint.
app.get('/config', (req, res) => {
    res.json({
      stripePublishableKey: process.env.STRIPE_PUBLISHABLE_KEY,
      stripeCountry: process.env.STRIPE_ACC_COUNTRY,
      country: process.env.STRIPE_COUNTRY,
      currency: process.env.STRIPE_CURRENCY,
      paymentMethods: paymentMethods,
      shippingOptions: shippingOptions,
    });
});

// Webhook handler to process payments for sources asynchronously.
app.post('/webhook', async (req, res) => {
    let data;
    let eventType;
    // Check if webhook signing is configured.
    if (process.env.STRIPE_WEBHOOK_SIGNATURE) {
      // Retrieve the event by verifying the signature using the raw body and secret.
      let event;
      let signature = req.headers['stripe-signature'];
      try {
        event = stripe.webhooks.constructEvent(
          req.rawBody,
          signature,
          process.env.STRIPE_WEBHOOK_SIGNATURE
        );
      } catch (err) {
        console.log(`⚠️  Webhook signature verification failed.`);
        return res.sendStatus(400);
      }
      // Extract the object from the event.
      data = event.data;
      eventType = event.type;
    } else {
      // Webhook signing is recommended, but if the secret is not configured in `config.js`,
      // retrieve the event data directly from the request body.
      data = req.body.data;
      eventType = req.body.type;
    }
    const object = data.object;
  
    // Monitor payment_intent.succeeded & payment_intent.payment_failed events.
    if (object.object === 'payment_intent') {
      const paymentIntent = object;
      if (eventType === 'payment_intent.succeeded') {
        console.log(
          `🔔  Webhook received! Payment for PaymentIntent ${paymentIntent.id} succeeded.`
        );
      } else if (eventType === 'payment_intent.payment_failed') {
        const paymentSourceOrMethod = paymentIntent.last_payment_error
          .payment_method
          ? paymentIntent.last_payment_error.payment_method
          : paymentIntent.last_payment_error.source;
        console.log(
          `🔔  Webhook received! Payment on ${paymentSourceOrMethod.object} ${paymentSourceOrMethod.id} of type ${paymentSourceOrMethod.type} for PaymentIntent ${paymentIntent.id} failed.`
        );
        // Note: you can use the existing PaymentIntent to prompt your customer to try again by attaching a newly created source:
        // https://stripe.com/docs/payments/payment-intents/usage#lifecycle
      }
    }
  
    // Monitor `source.chargeable` events.
    if (
      object.object === 'source' &&
      object.status === 'chargeable' &&
      object.metadata.paymentIntent
    ) {
      const source = object;
      console.log(`🔔  Webhook received! The source ${source.id} is chargeable.`);
      // Find the corresponding PaymentIntent this source is for by looking in its metadata.
      const paymentIntent = await stripe.paymentIntents.retrieve(
        source.metadata.paymentIntent
      );
      // Check whether this PaymentIntent requires a source.
      if (paymentIntent.status != 'requires_payment_method') {
        return res.sendStatus(403);
      }
      // Confirm the PaymentIntent with the chargeable source.
      await stripe.paymentIntents.confirm(paymentIntent.id, {source: source.id});
    }
  
    // Monitor `source.failed` and `source.canceled` events.
    if (
      object.object === 'source' &&
      ['failed', 'canceled'].includes(object.status) &&
      object.metadata.paymentIntent
    ) {
      const source = object;
      console.log(`🔔  The source ${source.id} failed or timed out.`);
      // Cancel the PaymentIntent.
      await stripe.paymentIntents.cancel(source.metadata.paymentIntent);
    }
  
    // Return a 200 success code to Stripe.
    res.sendStatus(200);
  });

// Start the server on the correct port.
const server = app.listen(process.env.PORT, () => {
  console.log(`🚀  Server listening on port ${server.address().port}`);
});

// Turn on the ngrok tunnel in development, which provides both the mandatory HTTPS
// support for all card payments, and the ability to consume webhooks locally.
if (ngrok) {
  ngrok
    .connect({
      addr: process.env.PORT,
      authtoken: process.env.NGROK_AUTH_TOKEN,
      region: process.env.NGROK_REGION
    })
    .then(url => {
      console.log(`💳  App URL to see the demo in your browser: ${url}/`);
    })
    .catch(err => {
      console.log(err);
      if (err.code === 'ECONNREFUSED') {
        console.log(`⚠️  Connection refused at ${err.address}:${err.port}`);
      } else {
        console.log(`⚠️ Ngrok error: ${JSON.stringify(err)}`);
      }
      process.exit(1);
    });
}